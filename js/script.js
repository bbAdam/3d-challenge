$(function() {
    SeekChallenge.Init();
});

let SeekChallenge = {
    
    "Init": function() {
        // FOR SCOPE
        let _this = this;
        
        this.QuizReveal();
        this.QuizActions();
        this.QuizReset();

        $(window).resize(function() { _this.WindowResize(); });
    },

    "WindowResize": function(){
        let quizTop = $(".quiz-questions").offset().top;
        let activeWidth = $(".quiz-questions").outerWidth();
        let activeWidthPercentage = .04 * activeWidth;

        $(".active-question-highlight").css({
            height: $(".fieldset-container.active").outerHeight(),
            width: $(".fieldset-container.active").outerWidth(),
            top: $(".fieldset-container.active").offset().top - quizTop,
            left: activeWidthPercentage
        })
    },

    "QuizReveal": function() {
        let _this = this;

        $(".take-quiz").on("click keydown", function(e){
            if(SeekChallenge.AllyClick(e)) {
                e.preventDefault();

                //REVEAL QUIZ
                $(this).attr("aria-expanded", "true");
                $(".quiz-questions").attr("aria-hidden","false").slideDown();

                //GET VALUES FOR ACTIVE QUESTION HIGHLIGHTER
                let activeWidth = $(".quiz-questions").outerWidth();
                let activeWidthPercentage = .04 * activeWidth;


                //SET INITIAL HEIGHT OF ACTIVE QUESTION HIGHLIGHTER
                setTimeout(function(){
                    $(".active-question-highlight").animate({
                        height: $(".fieldset-container.active").outerHeight(),
                        opacity: .75,
                        left: activeWidthPercentage,
                        width: $(".fieldset-container.active").outerWidth(),
                    }, 1000);

                    //SHOW QUIZ RESET BUTTON
                    $(".btn.reset-quiz").addClass("show");
                }, 100)
                
            }
        })
    },

    "QuizActions": function(){
        let _this = this;

        $(".fieldset-container input").on("change", function(){
            //SETUP VARIABLES TO CAPTURE CHOICE SELECTION AND TOP POSITION OF QUIZ
            let selectedInput = $(this).attr("name");
            let quizTop = $(".quiz-questions").offset().top;

            //REMOVE SELECTED CLASS FROM ALL OPTIONS WITHIN THE CURRENT FIELDSET AND ADD CLASS TO SELECTED OPTION
            $(this).closest(".fieldset-container").find("label").removeClass("selected");
            $(this).next().addClass("selected");

            //SHOW CALCULATING DISPLAY
            if(!$(".calculating").hasClass("active")){
                $(".calculating").slideDown().addClass("active");
            }

            if(selectedInput != "question3"){ //NOT ON THE FINAL QUESTION
                //SET CLASSES TO REFLECT CURRENT QUESTION
                $(this).closest(".fieldset-container").removeClass("active").next().addClass("active")

                //MOVE ACTIVE QUESTION HIGHLIGHT
                $(".active-question-highlight").animate({
                    top: $(".fieldset-container.active").offset().top - quizTop,
                    height: $(".fieldset-container.active").outerHeight()
                }, 1000);
            } else { //FINAL QUESTION

                //ADD UP THE ANSWERS
                let answerTotal = 0;
                $(".fieldset-container").each(function(){
                    let selectedAnswerValue = Number($(this).find(".selected").prev().attr("value"));
                    answerTotal += selectedAnswerValue;
                })

                //REVEAL ANSWER
                _this.QuizAnswer(answerTotal);   
            }            
        })

        //FORCE PSEUDO RADIO BUTTON TO BEHAVE LIKE REAL RADIO BUTTON
        $(".fieldset-container input").on("focus", function(){
            $(this).next().addClass("selected");
        })
    },

    "QuizReset": function(){
        $(".reset-quiz").on("click keydown", function(e){
            if(SeekChallenge.AllyClick(e)) {
                e.preventDefault();

                //FIND TOP POSITION OF QUIZ FOR HIGHLIGHTER POSITION RESET
                let quizTop = $(".quiz-questions").offset().top;

                //HIDE CALCULATING DISPLAY AND RESULTS
                $(".calculating, .answer").slideUp().removeClass("active");

                //CLEAR SHAPE SCENE
                shapeController.clearScene();

                //RESET RADIO BUTTONS
                $("input:radio[name=question1]:checked")[0].checked = false;
                $("input:radio[name=question2]:checked")[0].checked = false;
                $("input:radio[name=question3]:checked")[0].checked = false;

                //RESET QUIZ ANSWERS AND HIDE QUIZ
                $(".btn.take-quiz").attr("aria-expanded", "false");
                $(".quiz-questions").attr("aria-hidden","true").slideUp();
                $(".fieldset-container").removeClass("active");
                $(".fieldset-container:first-child").addClass("active");
                $("label").removeClass("selected");
                $(".active-question-highlight").animate({
                    height: $(".fieldset-container.active").outerHeight(),
                    opacity: 0,
                    top: $(".fieldset-container.active").offset().top - quizTop,
                    width: $(".fieldset-container.active").outerWidth(),
                }, 1000);
            }
        })
    },

    "QuizAnswer": function(answerTotal){
        $(".calculating").slideUp("slow", function(){
            if(answerTotal < 10){ //CUBE
                //SHOW ANSWER TEXT
                $(".answer.cube-type").slideDown(function(){
                    //CREATE SHAPE
                    setTimeout(function(){shapeController.createCube()}, 300);
                })
            } else if(answerTotal < 9000) { //CONE
                //SHOW ANSER TEXT
                $(".answer.cone-type").slideDown(function(){
                    //CREATE SHAPE
                    setTimeout(function(){shapeController.createCone()}, 300);
                })
            } else { //DELICIOUS
                //SHOW ANSWER TEXT
                $(".answer.delicious-type").slideDown();
            }
        });
        
    },

    "AllyClick": function(event) {
        //CHECK FOR CLICK EVENT AS WELL AS SPACEBAR AND ENTER KEY FOR KEYBOARD NAVIGATION
        if(event.type == "click") {
            return true;
        } else if(event.type == "keydown") {
            if(event.keyCode == 32 || event.keyCode == 13) {
                return true;
            }
        } else {
            return false;
        }
    }
};

//CREATE SOME GLOBAL VARIABLES FOR OUR SCENE, RENDERER, AND DOM SHAPE CONTAINER
const shapeContainer = document.querySelector(".shape");
const scene = new THREE.Scene();
const renderer = new THREE.WebGLRenderer({ alpha:true });
renderer.setSize( shapeContainer.offsetWidth, shapeContainer.offsetHeight );
shapeContainer.appendChild(renderer.domElement);


const shapeController = {
    //SHAPE PROPERTIES
    cubeShape: new THREE.BoxGeometry(),
    cubeSkin: new THREE.MeshBasicMaterial( { color: 0x9C5AB9 } ),
    cubeCamera: new THREE.PerspectiveCamera( 30, shapeContainer.offsetWidth / shapeContainer.offsetHeight, 0.1, 1000 ),
    coneShape: new THREE.CylinderGeometry(0.2, 0.5, 1, 16),
    coneSkin: new THREE.MeshBasicMaterial( { color: 0x68A6A5 } ),
    coneCamera: new THREE.PerspectiveCamera( 20, shapeContainer.offsetWidth / shapeContainer.offsetHeight, 0.1, 1000 ),

    //SHAPE METHODS
    clearScene: function(){ //REMOVE ANY OBJECTS THAT REMAIN WITHIN THE SCENE
        while(scene.children.length > 0){ 
            scene.remove(scene.children[0]); 
        }

        //REMOVE ANY EVENT LISTENERS OR RENDER TARGETS FROM THE RENDERER
        renderer.renderLists.dispose();  
    },
    createCube: function(){
        //CLEAR OUT THE SCENE BEFORE WE ADD A NEW SHAPE
        this.clearScene();

        if(this.cube == undefined){ //IF WE HAVENT CREATED A CUBE YET, MAKE ONE AND STORE IT
            this.cube = new THREE.Mesh( this.cubeShape, this.cubeSkin );
        }
        
        //PLACE THE CUBE INTO THE SCENE
        scene.add( this.cube );
        
        //ANIMATE THE CUBE
        this.animateCube();
    },
    animateCube: function(){
        //LOOP THROUGH THE ANIMATION FUNCTION INDEFINITELY
        requestAnimationFrame( this.animateCube.bind(this) );

        //CUBE ANIMATION PARAMETERS
        this.cube.rotation.x += 0.01;
        this.cube.rotation.y += 0.01;

        //ADD THE CUBE CAMERA AND SCENE INTO THE RENDERER
        renderer.render( scene, this.cubeCamera );
        this.cubeCamera.position.z = 5;
    },
    createCone: function(){
        //CLEAR OUT THE SCENE BEFORE WE ADD A NEW SHAPE
        this.clearScene();

        if(this.cone == undefined){ //IF WE HAVENT CREATED A CONE YET, MAKE ONE AND STORE IT
            this.cone = new THREE.Mesh( this.coneShape, this.coneSkin );
        }

        //PLACE THE CONE INTO THE SCENE
        scene.add( this.cone );

        //ANIMATE THE CONE
        this.animateCone();
    },
    animateCone: function(){
        //LOOP THROUGH THE ANIMATION FUNCTION INDEFINITELY
        requestAnimationFrame( this.animateCone.bind(this) );

        //CONE ANIMATION PARAMETERS
        this.cone.rotation.x += 0.01;
        this.cone.rotation.y += 0.01;

        //ADD THE CONE CAMERA AND SCENE INTO THE RENDERER
        renderer.render( scene, this.coneCamera );
        this.coneCamera.position.z = 5;
    }
}